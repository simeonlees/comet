import sys
from comet.controllers.load_data_controller import LoadDataController
from comet.controllers.preferences_controller import PreferencesController
from comet.views.load_data_view import LoadDataView
from comet.views.preferences_view import PreferencesView
import pandas as pd
from comet.models.session_model import SessionModel

import matplotlib.pyplot as plt

class MainController(object):

    def __init__(self, config, model):
        self.model = model
        self.config = config

    # called from view class
    # def change_running(self, checked):
    #     # put control logic here
    #     self.model.running = checked
    #     self.model.call_subscribed_funcs()
    #     print('controller knows shit checked')

    def quit_application(self):
        sys.exit()

    def load_data_dialog(self):
        self.load_data_controller = LoadDataController(self.config, self.model)
        self.load_data_view = LoadDataView(self.model, self.load_data_controller)
        self.load_data_view.show()

    def preferences_dialog(self):
        self.preferences_dialog_controller = PreferencesController(self.model)
        self.preferences_view = PreferencesView(self.model, self.preferences_dialog_controller)
        self.preferences_view.show()

    def trim_session_data(self, start, end):
        print('trimming')
        print('start')
        print(start)
        print('end')
        print(type(end))
        self.model.selected_session.session_data = self.model.selected_session.session_data.between_time(start.time(), end.time())
        self.model.call_subscribed_funcs()

    def detect_intervals(self):
        intervals = []
        # ema = self.model.selected_session.session_data['power'].rolling(window=20, win_type='triang').mean()
        # ema = self.model.selected_session.session_data['power'].ewm(span=30).mean()
        self.model.selected_session.session_data['ema'] = self.model.selected_session.session_data['power'].ewm(span=30).mean()
        self.model.selected_session.session_data['ema'] = self.model.selected_session.session_data['ema'].shift(-10)
        # print(self.model.selected_session.session_data)

        average_power = self.model.selected_session.session_data['power'].mean()
        is_interval = False

        for index, row in self.model.selected_session.session_data.iterrows():
            if row['ema'] > average_power:
                if not is_interval:
                    print('adding')
                    intervals.append(pd.DataFrame(data=None, columns=self.model.selected_session.session_data.columns))
                    is_interval = True
                intervals[len(intervals) - 1] = intervals[len(intervals)-1].append(row)
            else:
                is_interval = False

        for interval in intervals:
            print(len(interval))
            session_model = SessionModel()
            session_model.header = self.model.selected_session.header
            session_model.session_data = interval
            print('index')
            print(interval.index[0])
            # session_model.start_datetime = self.model.selected_session.start_datetime
            session_model.start_datetime = interval.index[0]
            session_model.session_data['normalisedpower'] = session_model.session_data['power'].rolling(window=30).mean()
            session_model.is_interval = True
            self.model.sessions.append(session_model)



        # ema = ema.shift(-10)
        com = self.model.selected_session.session_data['power'].ewm(com=30).mean()
        # print(type(ema))
        fig = plt.figure()
        ax = self.model.selected_session.session_data['power'].plot()
        # ax2 = ema.plot()
        ax3 = com.plot()
        plt.axhline(y=self.model.selected_session.session_data['power'].mean())
        plt.legend()
        plt.show()

        del self.model.selected_session.session_data['ema']

        self.model.call_subscribed_funcs()
