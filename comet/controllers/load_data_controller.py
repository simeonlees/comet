import pandas as pd
import datetime
from comet.models.session_model import SessionModel

class LoadDataController(object):

    config = {}

    def __init__(self, config, model):
        self.model = model
        exec(compile(open(config, "rb").read(), config, 'exec'), self.config)

    def parse_file(self, filename):

        session_model = SessionModel()
        is_header = False
        line_number = 0

        file = open(filename, 'r')

        for line in file:
            if is_header:
                # Stop reading if reach empty line
                if not line.strip():
                    is_header = False
                else:
                    session_model.header[line.split('=')[0]] = line.split('=')[1].strip()

            if line.strip() == '[Params]':
                is_header = True

            line_number += 1

            if line.strip() == '[HRData]':
                break

        data_column_names, is_hr_data_only, is_metric = self.parse_smode(session_model.header['SMode'])

        print(data_column_names)

        session_model.start_datetime = self.get_datetime_from_header_params(session_model.header['Date'],session_model.header['StartTime'])

        session_model.session_data = pd.read_table(
            skiprows=line_number,
            filepath_or_buffer=filename,
            header=None,
            names=data_column_names
        )


        for column in session_model.session_data:
            config_item = next((item for item in self.config['core_data_schema'] if item['key'] == column), None)
            if 'function' in config_item:
                # Apply correction function to column
                session_model.session_data[column] = config_item['function'](session_model.session_data[column])
            if not is_metric and 'convert_to_metric_factor' in config_item:
                # Convert to metric units
                session_model.session_data[column] = session_model.session_data[column] * config_item['convert_to_metric_factor']

        df_date_range = pd.date_range(
            session_model.start_datetime,
            periods= len(session_model.session_data),
            freq= '%sS' % session_model.header['Interval'])

        session_model.session_data.set_index(df_date_range, inplace=True)

        session_model.session_data['normalisedpower'] = session_model.session_data['power'].rolling(window=30).mean()

        print(session_model.session_data)

        session_model.is_interval = False

        self.model.sessions.append(session_model)
        self.model.selected_session = session_model

        self.model.call_subscribed_funcs()

    def parse_smode(self, smode):

        column_names = [d['key'] for d in self.config['core_data_schema']]

        is_hr_data_only = True
        is_metric = True

        matched_column_names = []
        flags = list(smode)

        for i, flag in enumerate(flags):
            if i == 6:
                if flag == '1':
                    is_hr_data_only = False
            elif i == 7:
                if flag == '1':
                    is_metric = False
            elif i == 8:
                if flag == '1':
                    matched_column_names.append(column_names[5])
            else:
                if flag == '1':
                    matched_column_names.append(column_names[i])

        return matched_column_names, is_hr_data_only, is_metric

    def get_datetime_from_header_params(self, date, time):
        datestring = '%s-%s'%(date,time)
        dt = datetime.datetime.strptime(datestring[:-2], "%Y%m%d-%H:%M:%S")
        return dt