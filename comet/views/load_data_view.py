import os
from PyQt4 import QtGui
from comet.views.generated.load_file_dialog import Ui_LoadDataDialog

class LoadDataView(QtGui.QDialog):

    def __init__(self, model, load_data_controller):
        self.model = model
        self.load_data_controller = load_data_controller
        super(LoadDataView, self).__init__()
        self.init_ui()
        # register func with model for future model update announcements
        self.model.subscribe_model_update_func(self.update_ui_from_model)
        self.accepted.connect(self.load_file)

    def init_ui(self):
        self.ui = Ui_LoadDataDialog()
        self.ui.setupUi(self)
        self.ui.chooseFileButton.clicked.connect(self.open_file)

    def open_file(self):
        self.file_name = QtGui.QFileDialog.getOpenFileName(self, 'Open File', filter='HRM files (*.hrm)')
        self.ui.fileNameLabel.setText(os.path.basename(self.file_name))
        file = open(self.file_name, 'r')
        self.ui.fileTextBrowser.setText(file.read())

    def load_file(self):
        self.load_data_controller.parse_file(self.file_name)

    def update_ui_from_model(self):
        """
        Function registered in model, will be called by model whenever relevant data changes so changes are reflected in view
        """
        pass
        # self.running = self.model.running