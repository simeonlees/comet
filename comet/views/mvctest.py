# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mvctest.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CometWindow(object):
    def setupUi(self, CometWindow):
        CometWindow.setObjectName(_fromUtf8("CometWindow"))
        CometWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(CometWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.pushButton_running = QtGui.QCheckBox(self.centralwidget)
        self.pushButton_running.setGeometry(QtCore.QRect(350, 160, 101, 51))
        self.pushButton_running.setObjectName(_fromUtf8("pushButton_running"))
        CometWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(CometWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        CometWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(CometWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        CometWindow.setStatusBar(self.statusbar)

        self.retranslateUi(CometWindow)
        QtCore.QMetaObject.connectSlotsByName(CometWindow)

    def retranslateUi(self, CometWindow):
        CometWindow.setWindowTitle(_translate("CometWindow", "MainWindow", None))
        self.pushButton_running.setText(_translate("CometWindow", "CheckBox", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    CometWindow = QtGui.QMainWindow()
    ui = Ui_CometWindow()
    ui.setupUi(CometWindow)
    CometWindow.show()
    sys.exit(app.exec_())

