from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import pandas as pd
import numpy as np
import datetime
from comet.views.generated.comet_gui import Ui_CometWindow
from comet.views.chart_view import ChartView
from .format_date import Format_Date
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)

class MainView(QtGui.QMainWindow):

    # properties to read/write widget value
    @property
    def date(self):
        return self.ui.date_label.text()

    @date.setter
    def date(self, value):
        self.ui.date_label.setText(value)

    @property
    def time(self):
        return self.ui.time_label.text()

    @time.setter
    def time(self, value):
        self.ui.time_label.setText(value)

    @property
    def interval(self):
        return self.ui.interval_label.text()

    @interval.setter
    def interval(self, value):
        self.ui.interval_label.setText(value)

    config = {}


    def __init__(self, config_path, model, main_controller):
        self.model = model
        self.main_controller = main_controller
        self.charts = []
        super(MainView, self).__init__()
        # Read in config file and store in variable
        exec(compile(open(config_path, "rb").read(), config_path, 'exec'), self.config)
        self.init_ui()
        # register func with model for future model update announcements
        self.model.subscribe_model_update_func(self.update_ui_from_model)

    def init_ui(self):
        self.ui = Ui_CometWindow()
        self.ui.setupUi(self)
        # connect signal to method
        # self.ui.pushButton_running.clicked.connect(self.on_running) ## Connects button to event
        self.ui.actionQuit.triggered.connect(self.quit_application)
        self.ui.actionLoad_data.triggered.connect(self.load_data_dialog)
        self.ui.actionPreferences.triggered.connect(self.preferences_dialog)
        self.ui.actionSplit_Intervals.triggered.connect(self.split_intervals_dialog)
        self.ui.calendarWidget.clicked[QtCore.QDate].connect(self.show_available_sessions)

    def quit_application(self):
        self.main_controller.quit_application()

    def load_data_dialog(self):
        self.main_controller.load_data_dialog()

    def preferences_dialog(self):
        self.main_controller.preferences_dialog()

    def split_intervals_dialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Question)
        msg.setText("Are you sure you want to split the session into distinct intervals?")
        msg.setWindowTitle("Split Session into Intervals")
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        retval = msg.exec_()
        if retval == QMessageBox.Ok:
            self.main_controller.detect_intervals()

    def generate_basic_charts(self, df):
        self.charts.clear()
        # print(list(self.model.session_data.columns.values))

        # Clear chart area
        for i in reversed(range(self.ui.chartArea.count())):
            self.ui.chartArea.itemAt(i).widget().setParent(None)

        for column in self.model.selected_session.session_data:

            item = next((item for item in self.config['core_data_schema'] if item['key'] == column), None)
            print(item)
            print(type(item))
            print(column)
            fig = Figure()
            ax = fig.add_subplot(111)

            if not self.model.settings.value('is_metric', type=bool) and 'imperial_units' in item:
                ax.plot(self.model.selected_session.session_data[column] / item['convert_to_metric_factor'])
                ax.set_ylabel(item['imperial_units'])
            else:
                ax.plot(self.model.selected_session.session_data[column])
                ax.set_ylabel(item['units'])

            ax.set_title(item['title'])
            cv1 = ChartView(self.charts, self.main_controller)
            cv1.add_mpl(fig)
            self.ui.chartArea.addWidget(cv1)

    def update_ui_from_model(self):
        """
        Function registered in model, will be called by model whenever relevant data changes so changes are reflected in view
        """

        self.generate_basic_charts(self.model.selected_session.session_data)
        self.set_session_header()
        self.init_table()
        self.init_calendar()


    def set_session_header(self):
        """
        Updates header information for the session such as date, time and interval length.
        """
        date = self.model.selected_session.start_datetime



        # self.date = date.strftime('%A %d %Y')
        self.date = Format_Date.long_date(self, date)
        self.time = date.strftime('%I:%M %p')
        self.interval = self.model.selected_session.header['Interval']

        dt = datetime.datetime.strptime(self.model.selected_session.header['Length'][:-2], "%H:%M:%S")
        hours = dt.hour + dt.minute / 60. + dt.second / 3600.




        self.ui.average_heart_rate_label.setText(str(round(self.model.selected_session.session_data['heartrate'].mean(),2)) + ' BPM')
        self.ui.max_heart_rate_label.setText(str(self.model.selected_session.session_data['heartrate'].max()) + ' BPM')
        self.ui.min_heart_rate_label.setText(str(self.model.selected_session.session_data['heartrate'].min()) + ' BPM')
        self.ui.average_power_label.setText(str(round(self.model.selected_session.session_data['power'].mean(),2)) + ' Watts')
        self.ui.max_power_label.setText(str(self.model.selected_session.session_data['power'].max()) + ' Watts')

        self.model.selected_session.session_data['normalisedp'] = self.model.selected_session.session_data['normalisedpower'] ** 4
        normalised_power = self.model.selected_session.session_data['normalisedp'].mean()
        normalised_power = normalised_power ** (1. / 4)
        print(normalised_power)
        del self.model.selected_session.session_data['normalisedp']
        # self.ui.normalised_power_label.setText(str(normalised_power + ' Watts'))
        self.ui.normalised_power_label.setText(
            str(round(normalised_power, 2)) + ' Watts')

        mean_lrb_pi = self.model.selected_session.session_data['powerbalanceandpedallingindex'].mean()

        b = '{0:016b}'.format(int(mean_lrb_pi))

        pi = b[:8]
        lrb = b[8:]

        print(int(pi, 2))
        self.ui.power_index_label.setText(str(int(pi, 2)))
        self.ui.left_right_balance.setText(str(int(lrb, 2)))
        print(int(lrb, 2))

        if self.model.settings.value('power_threshold', type=float):
            tss = normalised_power / self.model.settings.value('power_threshold', type=float)
            self.ui.intensity_factor_label.setText(str(round(tss,2)))
            self.ui.training_stress_score_label.setText(
                str(round(tss*100*hours,2))
            )

        if self.model.settings.value('is_metric', type=bool):
            self.ui.distance_label.setText(str(round(self.model.selected_session.session_data['speed'].mean()*hours, 2)) + ' KM')
            self.ui.average_altitude_label.setText(str(round(self.model.selected_session.session_data['altitude'].mean(),2)) + ' M')
            self.ui.max_altitude_label.setText(str(self.model.selected_session.session_data['altitude'].max()) + ' M')
            self.ui.average_speed_label.setText(str(round(self.model.selected_session.session_data['speed'].mean(),2)) + ' KM/H')
            self.ui.max_speed_label.setText(str(self.model.selected_session.session_data['speed'].max()) + ' KM/H')
        else:
            self.ui.distance_label.setText(
                str(round(self.model.selected_session.session_data['speed'].mean()*hours /1.60934, 2)) + ' M')
            self.ui.average_altitude_label.setText(
                str(round(self.model.selected_session.session_data['altitude'].mean() /0.3048, 2)) + ' FT')
            self.ui.max_altitude_label.setText(str(round(self.model.selected_session.session_data['altitude'].max() /0.3048,2)) + ' FT')
            self.ui.average_speed_label.setText(
                str(round(self.model.selected_session.session_data['speed'].mean()/1.60934, 2)) + ' MPH')
            self.ui.max_speed_label.setText(str(round(self.model.selected_session.session_data['speed'].max()/1.60934,2)) + ' MPH')

    def init_table(self):
        sd = self.model.selected_session.session_data

        self.ui.tableWidget.setRowCount(len(sd.index))
        self.ui.tableWidget.setColumnCount(len(sd.columns))

        headers = []


        for n, key in enumerate(list(sd)):
            index = 0
            imperial_divisor = 1
            config_item = next((item for item in self.config['core_data_schema'] if item['key'] == key), None)
            title = config_item['title']
            if('imperial_units' in config_item and self.model.settings.value('is_metric', type=bool) == False):
                imperial_divisor = config_item['convert_to_metric_factor']
                title += ' (' + config_item['imperial_units'] + ')'
            else:
                title += ' (' + config_item['units'] + ')'
            headers.append(title)
            print(key)
            for m, row in sd.iterrows():
                table_item = QtGui.QTableWidgetItem(str(round(row[key] / imperial_divisor,2))) #
                self.ui.tableWidget.setItem(index, n, table_item)
                index+=1

        self.ui.tableWidget.setHorizontalHeaderLabels(headers)

    def init_calendar(self):
        highlighted_cell = QtGui.QTextCharFormat()
        highlighted_cell.setFontWeight(100)
        highlighted_cell.setFontUnderline(True)
        d = self.model.selected_session.start_datetime
        self.ui.calendarWidget.setSelectedDate(QtCore.QDate(d.year, d.month, d.day))
        self.ui.calendarWidget.setDateTextFormat(QtCore.QDate(d.year, d.month, d.day), highlighted_cell)
        self.show_available_sessions(d)

    def show_available_sessions(self, date):
        self.ui.available_sessions_listWidget.clear()
        for i, session in enumerate(self.model.sessions):
            if Format_Date.epoch_days_qdate(self, date) == Format_Date.epoch_days(self, self.model.selected_session.start_datetime):
                if session.is_interval:
                    self.ui.available_sessions_listWidget.addItem('{} (Interval) - [{}]'.format(session.start_datetime.strftime('%H:%M %p'), i))
                else:
                    item = QListWidgetItem()
                    item.setText('{} - [{}]'.format(session.start_datetime.strftime('%H:%M %p'),i))
                    self.ui.available_sessions_listWidget.addItem(item)
        self.ui.available_sessions_listWidget.itemDoubleClicked[QtGui.QListWidgetItem].connect(self.load_session)

    def load_session(self, item):
        text = item.text()
        ind = text.index('[')
        str_ind = text[ind+1]
        print(str_ind)

        int_ind = int(str_ind)

        self.model.selected_session = self.model.sessions[int_ind]
        self.model.call_subscribed_funcs()
