from PyQt4 import QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from comet.views.generated.chart_widget import Ui_mplWidget

import matplotlib as plt

class ChartView(QtGui.QWidget):
    def __init__(self, charts, controller):
        super(ChartView, self).__init__()
        print('initing')
        self.init_ui()
        self.charts = charts
        self.controller = controller

    def init_ui(self):
        self.ui = Ui_mplWidget()
        self.ui.setupUi(self)

    def add_mpl(self, fig):
        canvas = FigureCanvas(fig)
        canvas.setMinimumSize(500,300)
        canvas.setMaximumHeight(400)
        canvas.mpl_connect('button_press_event', self.mouse_press)
        canvas.mpl_connect('button_release_event', self.mouse_release)

        self.charts.append(canvas)
        print(len(self.charts))
        self.ui.mplWrapper.addWidget(canvas)

        canvas.draw()

    def draw_random_chart(self):
        fig1 = Figure()
        ax1f1 = fig1.add_subplot(111)
        ax1f1.plot(np.random.rand(5))
        return fig1

    def mouse_press(self, event):
        for chart in self.charts:
            pass
        self.trim_start = plt.dates.num2date(event.xdata)

    def mouse_release(self, event):
        for chart in self.charts:
            pass
        w, h = self.charts[0].get_width_height()
        print(dir(self.charts[0]))
        print(type(self.charts[0]))
        print(w)
        print(event.xdata) # Chart data
        self.trim_end = plt.dates.num2date(event.xdata)

        if self.trim_end > self.trim_start:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Question)
            msg.setText("Are you sure you want to trim session data to these times?")
            msg.setInformativeText("Start: {}\nEnd: {}\n\nTotal Length: {}".format(
                self.trim_start.strftime('%H:%M %p'),
                self.trim_end.strftime('%H:%M %p'),
                (self.trim_end - self.trim_start)
            ))
            msg.setWindowTitle("Trim Session Data")
            msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            retval = msg.exec_()
            if retval == QMessageBox.Ok:
                self.trim_data()
            else:
                pass

    def trim_data(self):
        self.controller.trim_session_data(self.trim_start, self.trim_end)