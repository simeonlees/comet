import os
from PyQt4 import QtGui
from comet.views.generated.preferences_dialog import Ui_Dialog

class PreferencesView(QtGui.QDialog):

    def __init__(self, model, preferences_controller):
        self.model = model
        self.preferences_controller = preferences_controller
        super(PreferencesView, self).__init__()
        self.init_ui()
        # register func with model for future model update announcements


        self.accepted.connect(self.accept)





    def init_ui(self):
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.buttonBox.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.apply)
        self.ui.radioButton.setChecked(self.model.settings.value('is_metric', type=bool))
        self.ui.radioButton_2.setChecked(not self.model.settings.value('is_metric', type=bool))
        if self.model.settings.value('power_threshold', type=float):
            self.ui.power_threshold_spinbox.setValue(self.model.settings.value('power_threshold', type=float))


    def accept(self):
        self.apply()
        self.close()
        print('accepted')

    def apply(self):
        if self.ui.radioButton.isChecked() and self.model.settings.value('is_metric', type=bool) == False or self.ui.radioButton_2.isChecked() and self.model.settings.value('is_metric', type=bool) == True:
            self.model.settings.setValue('is_metric', not self.model.settings.value('is_metric', type=bool))
            self.model.call_subscribed_funcs()
        self.model.settings.setValue('power_threshold',self.ui.power_threshold_spinbox.value())
        print(self.model.settings.value('power_threshold', type=float))
        print('applied')
