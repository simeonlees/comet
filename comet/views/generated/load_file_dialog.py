# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'load_file_dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_LoadDataDialog(object):
    def setupUi(self, LoadDataDialog):
        LoadDataDialog.setObjectName(_fromUtf8("LoadDataDialog"))
        LoadDataDialog.resize(404, 270)
        self.verticalLayout = QtGui.QVBoxLayout(LoadDataDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.widget = QtGui.QWidget(LoadDataDialog)
        self.widget.setMinimumSize(QtCore.QSize(0, 25))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.chooseFileButton = QtGui.QPushButton(self.widget)
        self.chooseFileButton.setGeometry(QtCore.QRect(0, 0, 91, 23))
        self.chooseFileButton.setObjectName(_fromUtf8("chooseFileButton"))
        self.fileNameLabel = QtGui.QLabel(self.widget)
        self.fileNameLabel.setGeometry(QtCore.QRect(100, 0, 156, 23))
        self.fileNameLabel.setObjectName(_fromUtf8("fileNameLabel"))
        self.verticalLayout.addWidget(self.widget)
        self.fileTextBrowser = QtGui.QTextBrowser(LoadDataDialog)
        self.fileTextBrowser.setObjectName(_fromUtf8("fileTextBrowser"))
        self.verticalLayout.addWidget(self.fileTextBrowser)
        self.buttonBox = QtGui.QDialogButtonBox(LoadDataDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(LoadDataDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), LoadDataDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), LoadDataDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(LoadDataDialog)

    def retranslateUi(self, LoadDataDialog):
        LoadDataDialog.setWindowTitle(_translate("LoadDataDialog", "Load Data", None))
        self.chooseFileButton.setText(_translate("LoadDataDialog", "Choose File...", None))
        self.fileNameLabel.setText(_translate("LoadDataDialog", "No File selected", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    LoadDataDialog = QtGui.QDialog()
    ui = Ui_LoadDataDialog()
    ui.setupUi(LoadDataDialog)
    LoadDataDialog.show()
    sys.exit(app.exec_())

