import datetime
from PyQt4 import QtCore

class Format_Date:

    def long_date(self, date):
        if 4 <= date.day <= 20 or 24 <= date.day <= 30:
            date_suffix = "th"
        else:
            date_suffix = ["st", "nd", "rd"][date.day % 10 - 1]

        date_string = date.strftime('%A ')
        date_string += '{dt.day}{sf} '.format(dt=date, sf=date_suffix)
        date_string += date.strftime('%B, %Y')

        return date_string

    def epoch_days(self, date):
        return (date - datetime.datetime(1970,1,1)).days

    def epoch_days_qdate(self, qdate):
        return QtCore.QDate(1970, 1, 1).daysTo(qdate)