import sys

from PyQt4 import QtGui

from comet.views.comet_gui import Ui_CometWindow


class View:

    ui = None

    def __init__(self):
        self.app = QtGui.QApplication(sys.argv)
        CometWindow = QtGui.QMainWindow()
        self.ui = Ui_CometWindow()
        self.ui.setupUi(CometWindow)

        self.ui.readfile_button.clicked.connect(self.shout)


        CometWindow.show()
        sys.exit(self.app.exec_())


    def setHeader(self, header):
        self.ui.date_label.setText(header['Date'])
        self.ui.time_label.setText(header['StartTime'])
        self.ui.interval_label.setText(header['Interval'])

    def shout(self):
        print('hey')
