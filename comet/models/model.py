from PyQt4 import QtGui
from PyQt4.QtCore import QSettings
import pandas as pd

class Model(object):
    def __init__(self):
        self._model_update_funcs = []

        # variable placeholders
        self.settings = QSettings('foo','foo')
        self.settings.setValue('is_metric', True)
        self.sessions = []
        self.selected_session = None

    # subscribe a view method for updating
    def subscribe_model_update_func(self, func):
        if func not in self._model_update_funcs:
            self._model_update_funcs.append(func)

    # unsubscribe a view method for updating
    def unsubscribe_model_update_func(self, func):
        if func in self._model_update_funcs:
            self._model_update_funcs.remove(func)

    # update registered view methods
    def call_subscribed_funcs(self):
        for func in self._model_update_funcs:
            func()