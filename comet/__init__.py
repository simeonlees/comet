import sys
from PyQt4 import QtGui
from comet.models.model import Model
from comet.controllers.main_controller import MainController
from comet.views.main_view import MainView

class App(QtGui.QApplication):
    """
    Top-level application class responsible for initiating the MVC and displaying the GUI.
    """
    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        config = '../config/main.conf'
        self.model = Model()
        self.main_controller = MainController(config, self.model)
        self.main_view = MainView(config, self.model, self.main_controller)
        self.main_view.show()

if __name__ == '__main__':
    app = App(sys.argv)
    sys.exit(app.exec_())
