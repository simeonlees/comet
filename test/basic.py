import unittest
from comet.controllers.load_data_controller import LoadDataController
from comet.models.model import Model

class BasicTests(unittest.TestCase):
    def test_get_datetime_from_header_params(self):
        config = 'test.conf'
        model = Model()

        ldc = LoadDataController(config, model)
        dt = ldc.get_datetime_from_header_params('20130205','15:46:20.0')

        self.assertTrue(dt.year == 2013)
        self.assertTrue(dt.day == 5)

    def test_smode_processes_correctly(self):
        config = 'test.conf'
        model = Model()

        ldc = LoadDataController(config, model)

        data_column_names, is_hr_data_only, is_metric = ldc.parse_smode('111111100')

        self.assertEqual(data_column_names[0], 'heartrate')
        self.assertEqual(data_column_names[4], 'power')
        self.assertEqual(data_column_names[5], 'powerbalanceandpedallingindex')



if __name__ == '__main__':
    unittest.main()