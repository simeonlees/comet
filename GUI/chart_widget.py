# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chart_widget.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mplWidget(object):
    def setupUi(self, mplWidget):
        mplWidget.setObjectName(_fromUtf8("mplWidget"))
        mplWidget.resize(400, 300)
        self.verticalLayout_2 = QtGui.QVBoxLayout(mplWidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.mplWrapper = QtGui.QVBoxLayout()
        self.mplWrapper.setObjectName(_fromUtf8("mplWrapper"))
        self.verticalLayout_2.addLayout(self.mplWrapper)

        self.retranslateUi(mplWidget)
        QtCore.QMetaObject.connectSlotsByName(mplWidget)

    def retranslateUi(self, mplWidget):
        mplWidget.setWindowTitle(_translate("mplWidget", "Form", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    mplWidget = QtGui.QWidget()
    ui = Ui_mplWidget()
    ui.setupUi(mplWidget)
    mplWidget.show()
    sys.exit(app.exec_())

